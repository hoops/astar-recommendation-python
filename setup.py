#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : 深圳星河软通科技有限公司 A.Star
# @contact: astar@snowland.ltd
# @site: www.astar.ltd
# @file: setup.py
# @time: 2018/9/8 1:31
# @Software: PyCharm


from setuptools import setup, find_packages
from apihelper import __version__
from astartool.setuptool import load_install_requires
setup(
    name='astar-als',
    version=__version__,
    description=(
        'als for python'
    ),
    long_description=open('description.rst', encoding='utf8').read(),
    author='A.Star',
    author_email='astar@snowland.ltd',
    maintainer='A.Star',
    maintainer_email='astar@snowland.ltd',
    license='Apache v2.0',
    packages=find_packages(),
    platforms=["all"],
    url='http://astar.ltd:10010/algorithm/astar-quantizationstrategy-python',
    classifiers=[
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: Implementation',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries'
    ],
    install_requires=load_install_requires()
)
